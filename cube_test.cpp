
#include "cube.h"
#include <gtest/gtest.h>

TEST(CubeTest, NaturalNumbers) {
   EXPECT_EQ(125, cube(5));
   EXPECT_EQ(0, cube(0));
}

TEST(CubeTest, NegativeNumbers) {
   EXPECT_EQ(-15625, cube(-25));
}

int main(int argc, char **argv) {
   ::testing::InitGoogleTest(&argc, argv);
   return RUN_ALL_TESTS();
}

      
