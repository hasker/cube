// cube.h

#ifndef _cube_h
#define _cube_h

// return x to the third power
int cube(int x);

#endif
