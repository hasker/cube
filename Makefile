# makefile for cube code

all: cube cube_test

test: cube_test
	./cube_test

cube.o: cube.h cube.cpp
	g++ -std=c++14 -Wall -c cube.cpp

cube_test.o: cube.h cube_test.cpp
	g++ -std=c++14 -Wall -c cube_test.cpp

main.o: cube.h main.cpp
	g++ -std=c++14 -Wall -c main.cpp

cube: cube.o main.o
	g++ -std=c++14 -Wall cube.o main.o -o cube

cube_test: cube.o cube_test.o
	g++ -pthread -std=c++14 -Wall cube.o cube_test.o -lgtest_main -lgtest -lpthread -o cube_test

clean:
	rm -f *.o cube cube_test 
