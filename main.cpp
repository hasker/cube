//
// main.cpp: prompt user for a number and print its cube
//

#include "cube.h"
#include <iostream>
using namespace std;

int main() {
   cout << "Enter number: ";
   int num;
   cin >> num;
   cout << "Cubed: " << cube(num) << endl;
   return 0;
}
