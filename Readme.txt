Sample code to illustrate a testing environment built on top of Docker.
Running the tests: 

        docker build --progress=plain ./

The --progress=plain option makes Docker show the run output on the screen. On a professional
setup you would be unlikely to use this option because all you care is that the tests pass,
but for students the extra output is helpful. If you get an unrecognized option error, just 
do the "docker build ./".